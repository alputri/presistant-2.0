from django.db import models


# Create your models here.
class Event(models.Model):
    id = models.IntegerField(editable=False, primary_key=True)
    name = models.TextField(max_length=30)
    place = models.TextField(max_length=30)
    day = models.TextField(max_length=9)
    startTime = models.IntegerField(editable=False)


class Schedule(models.Model):
    OWNER_CHOICES = [
        ('P', 'President'),
        ('VP', 'Vice President')
    ]

    id = models.IntegerField(editable=False, primary_key=True)
    owner = models.TextField(max_length=2, choices=OWNER_CHOICES)