from django.apps import AppConfig


class MainModelsConfig(AppConfig):
    name = 'main_models'
