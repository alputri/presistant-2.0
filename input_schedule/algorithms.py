import random


class Event:
    def __init__(self, id, name, place, day, startTime):
        self.id = id
        self.name = name
        self.place = place
        self.day = day
        self.startTime = startTime


class Schedule:
    def __init__(self, id, owner):
        self.id = id
        self.owner = owner


class Individual:
    def __init__(self, individual, fitness):
        self.individual = individual
        self.fitness = fitness


def initializePopulation(events, schedules):
    population = []

    for x in range(5):
        individual = {schedules[0]: [], schedules[1]: []}
        for y in range(len(events)):
            random.shuffle(schedules)
            individual.get(schedules[0]).append(events[y])

        population.append(individual)
    return population


def calculateFitness(individual, schedules):
    tempCounter = []
    currSched = individual.get(schedules[0])
    fitness = len(currSched)

    for x in range(len(currSched)):
        if currSched[x].id not in tempCounter:
            for y in range(x + 1, len(currSched)):
                if (currSched[x].day == currSched[y].day) and \
                        (currSched[x].startTime <= currSched[y].startTime < currSched[x].startTime + 1):
                    tempCounter.append(currSched[y].id)
                    tempCounter.append(currSched[x].id)

    currSched = individual.get(schedules[1])
    fitness += len(currSched)

    for x in range(len(currSched)):
        if currSched[x].id not in tempCounter:
            for y in range(x + 1, len(currSched)):
                if (currSched[x].day == currSched[y].day) and \
                        (currSched[x].startTime <= currSched[y].startTime < currSched[x].startTime + 1):
                    tempCounter.append(currSched[y].id)
                    tempCounter.append(currSched[x].id)

    tempCounter = set(tempCounter)
    fitness += len(tempCounter) * -1

    res = Individual(individual, fitness)
    return res


def selectContenders(lstIndividuals):
    indexes = random.sample(range(0, len(lstIndividuals)), 4)

    contenders = []

    if (lstIndividuals[indexes[0]].fitness > lstIndividuals[indexes[1]].fitness):
        contenders.append(lstIndividuals[indexes[0]])
    else:
        contenders.append((lstIndividuals[indexes[1]]))

    if (lstIndividuals[indexes[2]].fitness > lstIndividuals[indexes[3]].fitness):
        contenders.append(lstIndividuals[indexes[2]])
    else:
        contenders.append((lstIndividuals[indexes[3]]))

    return contenders


def crossoverMutation(contenders, schedules):
    con1 = contenders[0]
    con2 = contenders[1]

    tempInd1 = {schedules[0]: con1.individual.get(schedules[0]), schedules[1]: con2.individual.get(schedules[1])}
    tempInd2 = {schedules[0]: con2.individual.get(schedules[0]), schedules[1]: con1.individual.get(schedules[1])}

    if len(tempInd1.get(schedules[0])) != 0 and len(tempInd1.get(schedules[1])) != 0 and \
            len(tempInd2.get(schedules[0])) != 0 and len(tempInd2.get(schedules[1])) != 0:
        tempValue = tempInd1.get(schedules[0])[0]
        tempInd1.get(schedules[0])[0] = tempInd1.get(schedules[1])[0]
        tempInd1.get(schedules[1])[0] = tempValue

        tempValue = tempInd2.get(schedules[0])[0]
        tempInd2.get(schedules[0])[0] = tempInd2.get(schedules[1])[0]
        tempInd2.get(schedules[1])[0] = tempValue
    else:
        if (len(tempInd1.get(schedules[0])) == 0 or len(tempInd1.get(schedules[1])) == 0) and \
                (len(tempInd2.get(schedules[0])) == 0 or len(tempInd1.get(schedules[1])) == 0):

            tempSched = tempInd1.get(schedules[0])
            tempInd1[schedules[0]] = tempInd1.get(schedules[1])
            tempInd1[schedules[1]] = tempSched

            tempSched = tempInd2.get(schedules[0])
            tempInd2[schedules[0]] = tempInd2.get(schedules[1])
            tempInd2[schedules[1]] = tempSched

        elif len(tempInd1.get(schedules[0])) == 0 or len(tempInd1.get(schedules[1])) == 0:
            tempSched = tempInd1.get(schedules[0])
            tempInd1[schedules[0]] = tempInd1.get(schedules[1])
            tempInd1[schedules[1]] = tempSched

            tempValue = tempInd2.get(schedules[0])[0]
            tempInd2.get(schedules[0])[0] = tempInd2.get(schedules[1])[0]
            tempInd2.get(schedules[1])[0] = tempValue
        else:
            tempSched = tempInd2.get(schedules[0])
            tempInd2[schedules[0]] = tempInd2.get(schedules[1])
            tempInd2[schedules[1]] = tempSched

            tempValue = tempInd1.get(schedules[0])[0]
            tempInd1.get(schedules[0])[0] = tempInd1.get(schedules[1])[0]
            tempInd1.get(schedules[1])[0] = tempValue

    newInd1 = calculateFitness(tempInd1, schedules)
    newInd2 = calculateFitness(tempInd2, schedules)

    return [newInd1, newInd2]


def geneticAlgorithm(population, schedules):
    overallPopulation = []

    for x in population:
        overallPopulation.append(calculateFitness(x, schedules))

    for i in range(10):
        contenders = selectContenders(overallPopulation)
        overallPopulation.extend(crossoverMutation(contenders, schedules))

    overallPopulation.sort(key=lambda individual: individual.fitness, reverse=True)

    return overallPopulation[0]
