from django.shortcuts import render
from django.views.decorators.csrf import csrf_exempt
from .algorithms import *

response = {}

# Create your views here.
def index(request):
    html = "input_schedule.html"
    return render(request, html)

@csrf_exempt
def displayResults(request):
    eventCount = request.POST['count']
    events = []

    for x in range(int(eventCount)):
        id = x + 1
        name = request.POST['name' + str(id)]
        place = request.POST['loc' + str(id)]
        day = request.POST['day' + str(id)]

        timeStr = request.POST['time' + str(id)]
        time = int(timeStr)

        event = Event(id, name, place, day, time)
        events.append(event)

    schedules = [Schedule(1, 'P'), Schedule(2, 'VP')]

    pop = initializePopulation(events, schedules)
    res = geneticAlgorithm(pop, schedules)

    response['president'] = set(res.individual.get(schedules[0]))
    response['vice_president'] = set(res.individual.get(schedules[1]))

    html = "schedule_result.html"

    return render(request, html, response)
